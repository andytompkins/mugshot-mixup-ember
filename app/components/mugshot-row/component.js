//////// mugshot-row

import Ember from 'ember';

export default Ember.Component.extend({
    tagName: '',
    lastIndex: Ember.computed('game', function() {
        let game = this.get('game');
        let len = game.puzzle[0].length;
        //console.log('-----');
        //console.dir(game.puzzle[0]);
        //console.log('len = ' + len);
        return (len - 1);
    })
});
