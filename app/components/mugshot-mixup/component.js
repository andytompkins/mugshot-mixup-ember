///////// mugshot-mixup

import Ember from 'ember';

export default Ember.Component.extend({
    playing: false,
    difficulty: 10,
    actions: {
        startGame(difficulty) {
            console.log("start game at level " + difficulty);
            this.set('difficulty', difficulty);
            this.set('playing', true);
        },
        endGame() {
            this.set('playing', false);
        }
    }
    
});
