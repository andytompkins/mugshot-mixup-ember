///////// mugshot-game

import Ember from 'ember';
import Game from 'mugshot-mixup/Game';

const mugshots = 14;
const pieces = 3;

let mugs = new Array(pieces);
for (let i = 0; i < pieces; i++) {
  mugs[i] = new Array(mugshots);
  let pos = '';
  if (i === 0) {
    pos = 'top';
  } else if (i === 1) {
    pos = 'mid';
  } else {
    pos = 'bot';
  }
  for (let j = 1; j <= mugshots; j++) {
    let filename = 'mug' + j + '-' + pos + '.png';
    let image = 'assets/images/faces/' + filename;
    mugs[i][j-1] = {
      'image': image,
      'mug': j
    };
  }
}

export default Ember.Component.extend({
    time: 30,
    init() {
        this._super();
        let d = this.get('difficulty');
        //console.log("mugshot-game init, diff = " + d);
        let game = Game.create({
            mugs: mugs,
            difficulty: d
        });
        this.set('game', game);
        this.set('time', 30);
        
        this.tick = this.tick.bind(this);
    },
    keyUp(e) {
        console.log('got key ' + e.which);
        let game = this.get('game');
        if(!game.over) {
      
          if (e.which === 38 || e.which === 87) {
            //changeReelSound.play();
            game.reel_up();
          } else if (e.which === 40 || e.which === 83) {
            //changeReelSound.play();
            game.reel_down();
          } else if (e.which === 39 || e.which === 68) {
            //moveSound.play();
            game.reel_right();
          } else if (e.which === 37 || e.which === 65) {
            //moveSound.play();
            game.reel_left();
          }
          //this.setState({ 'game': this.state.game });
          //this.set('game', game);
      
        } else {
      
          if (e.which === 89) {
            this.newGame();
          } else if (e.which === 78) {
            this.get('onEnd')();
          }
      
        }
    },
    didInsertElement() {
        this.$('.index').focus();
        this.set('interval', window.setInterval(this.tick, 1000));
    },
    didRender() {
        
    },
    newGame() {
        let game = Game.create({
            mugs: mugs,
            difficulty: this.get('difficulty')
        });
        this.set('game', game);
        this.set('time', 30);
    },
    tick() {
        let game = this.get('game');
        if (game.over === true) {
          window.clearInterval(this.get('interval'));
        }
        let t = this.get('time');
        t--;
        this.set('time', t);
        
        if (t === 0) {
          game.game_over(false);
          window.clearInterval(this.get('interval'));
        }
    }
});
