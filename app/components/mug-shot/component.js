////////////// mug-shot

import Ember from 'ember';

export default Ember.Component.extend({
    tagName: '',
    init() {
        this._super();
        let col = this.get('col');
        let game = this.get('game');
        let row = this.get('row');
        if (col === 1 && game.selected_reel === row) {
            this.set('sel', 'selection');
        } else {
            this.set('sel', '');
        }
        //console.dir(game.puzzle);
    },
    imagePath: Ember.computed('col', 'row', 'game.puzzle[row][col].image', function() {
        let col = this.get('col');
        let game = this.get('game');
        let row = this.get('row');
        console.log("row and col are: " + row + " " + col);
        if (game && game.puzzle) {
            console.dir(game.puzzle);
            return game.puzzle[row][col].image;
        }
        return '';
    })
});

