///////// splash-screen

import Ember from 'ember';

export default Ember.Component.extend({
    difficulty: 10,
    didInsertElement: function() {
        this.$('.splash-component').focus();
    },
    easy: true,
    medium: false,
    hard: false,
    setFlags: function() {
        this.set('easy', false);
        this.set('medium', false);
        this.set('hard', false);
        if (this.difficulty === 10) { this.set('easy', true); }
        if (this.difficulty === 12) { this.set('medium', true); }
        if (this.difficulty === 14) { this.set('hard', true); }
    },
    keyUp: function(e) {
        let diff = 0;
        if (e.which === 13) {
          this.get('onStart')(this.difficulty);
        } else if (e.which === 38 || e.which === 87) {
          diff = this.difficulty - 2;
          if (diff < 10) { diff = 10; }
          this.difficulty = diff;
          this.setFlags();
        } else if (e.which === 40 || e.which === 83) {
          diff = this.difficulty + 2;
          if (diff > 14) { diff = 14; }
          this.difficulty = diff;
          this.setFlags();
        } 
    }
});
