import Ember from 'ember';

export default Ember.Route.extend({
  model() {
      const appState = {
          playing: false,
          difficulty: 10
      };

    return appState;
  }
});
