/**
 * Initializer for configuration management.
 *
 * Fetches config settings, checks query parameters for config overrides,
 * processes the data using GC's ClientConfig service, and injects the processed data into routes.
 */

const ClientConfig = new GrandCentral.ClientConfigService();
const Utilities = new GrandCentral.UtilitiesService();

function getJSON(url) {
  // Since the logic in this initializer is ultimately recommended for use during preflight,
  // GC's XHRPromise is used here instead of Ember's RSVP.Promise / $.getJSON.
  return new Utilities.XHRPromise('GET', url)
    .open()
    .then(result => result.response);
}

function getConfigOverrides() {
  const queryParams = Utilities.parseQueryString(location.search);

  // Create a list of override objects; GC will merge them all together for us.
  const configOverrides = Object.keys(queryParams)
    .filter(key => /^config:/.test(key))
    .map(key => {
      // 1. Slice off the key's 'config:' prefix.
      const configKey = key.slice('config:'.length);

      // 2. Attempt to parse the value (e.g., to turn 'false' into a boolean), falling back to the string.
      let configValue;
      try {
        configValue = JSON.parse(queryParams[key]);
      } catch (e) {
        configValue = queryParams[key];
      }

      // 3. Interpret dotted keys (e.g., to turn 'foo.bar=baz' into { foo: { bar: 'baz' }).
      return configKey.split('.').reduceRight((currentValue, nextKeyPart) => {
        return { [nextKeyPart]: currentValue };
      }, configValue);
    });

  return configOverrides;
}

function processConfig(settings) {
  // 1. Add any criteria verifiers. (Here, we use a fake locale as an example.)
  const locale = Utilities.parseQueryString(location.search).locale || 'xx-YY';
  ClientConfig.addCriteriaVerifier('locale', criterion => ClientConfig.matchesRegExp(criterion, locale));

  // 2. Add the raw settings from the server.
  ClientConfig.addSettings(settings);

  // 3. Use query parameters of format 'config:foo=bar' as config overrides.
  //    (NOT FOR USE IN PRODUCTION. Here, we use a config field as a guard.)
  if (!settings.isCustomerFacing) {
    const overrides = getConfigOverrides();
    ClientConfig.addSettings(overrides);
  }

  // 4. Compute final settings based on criteria and overrides.
  return ClientConfig.computeSettings();
}

// This function is based on the Valkyrie Preflight 'prefetch-service' initializer.
export function initialize(application) {
  application.deferReadiness();

  getJSON('config/app.json')
    .catch(() => {
      console.error('Failed to load config file!');
      return {};
    })
    .then(processConfig)
    .then(config => {
      application.register('data:client-config', config, { instantiate: false });
      application.inject('route', 'config', 'data:client-config');

      application.advanceReadiness();
    });
}

export default {
  name: 'client-config',
  initialize
};
