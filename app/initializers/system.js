const System = new GrandCentral.SystemService();

export function initialize(application) {
  System.init();
  application.set('ready', System.ready);
}

export default {
  name: 'system',
  initialize
};
