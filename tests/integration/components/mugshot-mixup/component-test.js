import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('mugshot-mixup', 'Integration | Component | mugshot mixup', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{mugshot-mixup}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#mugshot-mixup}}
      template block text
    {{/mugshot-mixup}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
