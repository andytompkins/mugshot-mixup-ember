# ClientConfig Ember Demo

Please read [this PR](https://github.snei.sony.com/SNEI/gc-client-config-demo/pull/1) for a description of this repo's purpose and usage.

## Setup

### Install

```bash
npm install -g bower ember-cli
git clone git@github.snei.sony.com:SNEI/gc-client-config-demo.git
cd gc-client-config-demo
npm install && bower install
```

### Launch

```bash
ember server
```

Then navigate to http://localhost:4200/.
